module.exports = async Kingdoms => {
  try {
    let reactionRolesGroupModels = await Kingdoms.database.models.reactionRolesGroup.findAll({
      attributes: [ 'messageID', 'channelID', 'guildID' ]
    });

    let reactionRolesGroups = reactionRolesGroupModels.map(model => model.dataValues);

    for (let group of reactionRolesGroups) {
      if (!Kingdoms.channels.has(group.channelID)) return;
      let channel = Kingdoms.channels.get(group.channelID);
      if (!channel.messages.has(group.messageID)) {
        await channel.fetchMessage(group.messageID);
      }
    }
  }
  catch (e) {
    Kingdoms.log.error(e);
  }
};
