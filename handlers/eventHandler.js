const fs = xrequire('fs');
const path = xrequire('path');

/**
 * Handles/Loads all the events.
 * @module eventHandler
 * @param {object} Kingdoms The Kingdoms Object.
 * @returns {void}
 */
module.exports = Kingdoms => {
  /* eslint-disable no-sync */
  let events = fs.readdirSync('./events/').
    filter(file => !fs.statSync(path.resolve('./events/', file)).isDirectory()).
    filter(file => file.endsWith('.js'));
  /* eslint-enable no-sync */

  for (let event of events) {
    event = event.replace(/\.js$/i, '');

    if (event === 'ready') {
      Kingdoms.on(event, () => xrequire('./events', event)(Kingdoms));
    }
    else {
      Kingdoms.on(event, xrequire('./events', event));
    }
  }
};
