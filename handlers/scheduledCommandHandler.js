const CronJob = xrequire('cron').CronJob;
const parseArgs = xrequire('command-line-args');

/**
 * Handles Kingdoms's scheduled commands
 * @param {Kingdoms} Kingdoms Discord client object
 * @returns {void}
 */
module.exports = Kingdoms => {
  setTimeout(async () => {
    try {
      let scheduledCommandModel = await Kingdoms.database.models.scheduledCommand.findAll({
        attributes: [ 'channelID', 'messageID', 'cronExp', 'command', 'arguments' ]
      });

      if (!scheduledCommandModel.length) return;

      for (let i = 0; i < scheduledCommandModel.length; i++) {
        let cronExp = scheduledCommandModel[i].dataValues.cronExp,
          command = scheduledCommandModel[i].dataValues.command.toLowerCase(), cmd,
          channel = Kingdoms.channels.get(scheduledCommandModel[i].dataValues.channelID);
        if (!channel) {
          removeScheduledCommandByChannelID(Kingdoms, scheduledCommandModel[i].dataValues.channelID);
          continue;
        }
        let args = scheduledCommandModel[i].dataValues.arguments ? scheduledCommandModel[i].dataValues.arguments.split(' ') : '';

        let job = new CronJob(cronExp,
          async function () {
            let message = await channel.fetchMessage(scheduledCommandModel[i].dataValues.messageID).catch(e => {
              if (e.toString().includes('Unknown Message')) {
                job.stop();
                removeScheduledCommandByMessageID(Kingdoms, scheduledCommandModel[i].dataValues.messageID);
              }
              else {
                Kingdoms.log.error(e);
              }
            });

            if (Kingdoms.commands.has(command)) {
              cmd = Kingdoms.commands.get(command);
            }
            else if (Kingdoms.aliases.has(command)) {
              cmd = Kingdoms.commands.get(Kingdoms.aliases.get(command).toLowerCase());
            }
            else {
              job.stop();
              return removeScheduledCommandByCommandName(Kingdoms, command);
            }

            if (cmd.config.enabled) {
              cmd.exec(Kingdoms, message, parseArgs(cmd.config.argsDefinitions, { argv: args, partial: true }));
            }
          },
          function () {},
          false // Start the job right now
        );
        job.start();
      }
    }
    catch (e) {
      Kingdoms.log.error(e);
    }
  }, 5 * 1000);
};

/**
 * Removes Kingdoms' scheduled commands
 * @param {Kingdoms} Kingdoms Discord client object
 * @param {String} channelID The Snowflake ID of the channel where the command is scheduled
 * @returns {void}
 */
function removeScheduledCommandByChannelID(Kingdoms, channelID) {
  Kingdoms.database.models.scheduledCommand.destroy({
    where: {
      channelID: channelID
    }
  }).catch(e => {
    Kingdoms.log.error(e);
  });
}

/**
 * Removes Kingdoms' scheduled commands
 * @param {Kingdoms} Kingdoms Discord client object
 * @param {String} messageID The Snowflake ID of the message that holds the scheduled command's info
 * @returns {void}
 */
function removeScheduledCommandByMessageID(Kingdoms, messageID) {
  Kingdoms.database.models.scheduledCommand.destroy({
    where: {
      messageID: messageID
    }
  }).catch(e => {
    Kingdoms.log.error(e);
  });
}

/**
 * Removes Kingdoms's scheduled commands
 * @param {Kingdoms} Kingdoms Discord client object
 * @param {String} commandName The name of the command that is scheduled
 * @returns {void}
 */
function removeScheduledCommandByCommandName(Kingdoms, commandName) {
  Kingdoms.database.models.scheduledCommand.destroy({
    where: {
      command: commandName
    }
  }).catch(e => {
    Kingdoms.log.error(e);
  });
}
