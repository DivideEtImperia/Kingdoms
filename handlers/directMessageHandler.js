/**
 * Handles direct messages sent to KingdomsBot
 * @param {Message} message Discord.js message object
 * @returns {void}
 */
module.exports = message => {
  if (!message.content) return;

  if (message.content.toLowerCase().startsWith('help')) {
    return message.channel.send({
      embed: {
        color: message.client.colors.BLUE,
        title: 'The Kingdoms Bot',
        url: 'http://home.kingdoms.network/',
        description: 'Join [**Kingdoms of Old**](http://discord.kingdoms.network) to play Kingdoms, test the bot and it\'s commands, for giveaway events, for chatting and for a lot of fun!',
        fields: [
          {
            name: 'Kingdoms Invite Link',
            value: 'http://discord.kingdoms.network'
          },
          {
            name: 'Kingdoms Bot Invite Link',
            value: `https://discordapp.com/oauth2/authorize?client_id=${message.client.user.id}&scope=bot&permissions=2146958463`
          }
        ],
        thumbnail: {
          url: message.client.user.displayAvatarURL
        },
        footer: {
          text: '</> with ❤ by Jeremy (a.k.a. DivideEtImperia)'
        }
      }
    }).catch(e => {
      message.client.log.error(e);
    });
  }
};
