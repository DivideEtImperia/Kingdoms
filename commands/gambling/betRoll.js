let recentUsers = [];

exports.exec = async (Kingdoms, message, args) => {
  try {
    let cooldown = 60;

    if (!recentUsers.includes(message.author.id)) {
      if (!args.money || args.money < 1 || !/^(one|two|three|four|five|six)$/i.test(args.outcome)) {
        /**
        * The command was ran with invalid parameters.
        * @fires commandUsage
        */
        return Kingdoms.emit('commandUsage', message, this.help);
      }

      args.money = parseInt(args.money);

      let minAmount = 5;
      if (args.money < minAmount) {
        /**
        * Error condition is encountered.
        * @fires error
        */
        return Kingdosm.emit('error', '', Kingdoms.i18n.error(message.guild.language, 'minBet', minAmount), message.channel);
      }

      let outcomes = [
        'one',
        'two',
        'three',
        'four',
        'five',
        'six'
      ];
      let outcome = outcomes[Math.floor(Math.random() * outcomes.length)];

      let guildMemberModel = await message.client.database.models.guildMember.findOne({
        attributes: [ 'kingdomCoins' ],
        where: {
          userID: message.author.id,
          guildID: message.guild.id
        }
      });

      guildMemberModel.dataValues.kingdomCoins = parseInt(guildMemberModel.dataValues.kingdomCoins);

      if (args.money > guildMemberModel.dataValues.kingdomCoins) {
        /**
        * Error condition is encountered.
        * @fires error
        */
        return Kingdoms.emit('error', '', Kingdoms.i18n.error(message.guild.language, 'insufficientBalance', guildMemberModel.dataValues.kingdomCoins), message.channel);
      }

      recentUsers.push(message.author.id);

      let result;
      if (outcome.toLowerCase() === args.outcome.toLowerCase()) {
        let prize = args.money < 50 ? args.money + outcomes.length : args.money < 100 ? args.money : args.money * 2;
        result = `Congratulations! You won the bet.\nYou won **${prize}** Kingdom Coins.`;

        /**
        * User's account is debited with Kingdom Coins
        * @fires userDebit
        */
        Kingdoms.emit('userDebit', message.member, prize);
      }
      else {
        result = 'Sorry, you lost the bet. Better luck next time.';

        /**
        * User's account is credited with Kingdom Coins
        * @fires userCredit
        */
        Kingdoms.emit('userCredit', message.member, args.money);
      }

      await message.channel.send({
        embed: {
          color: Kingdoms.colors.BLUE,
          title: `Rolled :${outcome}:`,
          description: result
        }
      });

      setTimeout(() => {
        recentUsers.splice(recentUsers.indexOf(message.author.id), 1);
      }, cooldown * 1000);
    }
    else {
      /**
      * Error condition is encountered.
      * @fires error
      */
      return Kingdoms.emit('error', '', Kingdoms.i18n.error(message.guild.language, 'gamblingCooldown', message.author, cooldown), message.channel);
    }
  }
  catch (e) {
    Kingdoms.log.error(e);
  }
};

exports.config = {
  aliases: [ 'br' ],
  enabled: true,
  argsDefinitions: [
    { name: 'outcome', type: String, alias: 'o', defaultOption: true },
    { name: 'money', type: Number, alias: 'm' }
  ]
};

exports.help = {
  name: 'betRoll',
  description: 'Bet %currency.name_plural% on prediction of the outcome of rolling a dice. If you win, you get more of it. But if you lose, you lose the amount you have bet.',
  botPermission: '',
  userTextPermission: '',
  userVoicePermission: '',
  usage: 'betroll < one/two/three/four/five/six > <-m amount>',
  example: [ 'betroll three -m 100' ]
};
