exports.exec = (Kingdoms, message, args) => {
  try {
    if (args.length < 1) {
      /**
      * The command was ran with invalid parameters.
      * @fires commandUsage
      */
      return Kingdoms.emit('commandUsage', message, this.help);
    }

    args = JSON.parse(args.join(' '));
    args.footer = {
      text: `${Kingdoms.credentials.ownerId.includes(message.author.id) ? '' : Kingdoms.i18n.info(message.guild.language, 'endorsementMessage')}`
    };

    message.channel.send({
      embed: args
    }).then(() => {
      if (message.deletable) {
        message.delete().catch(e => {
          Kingdoms.log.error(e);
        });
      }
    }).catch(e => {
      Kingdoms.log.error(e);
    });
  }
  catch (e) {
    /**
     * Error condition is encountered.
     * @fires error
     */
    return Kingdoms.emit('error', '', `${Kingdoms.i18n.error(message.guild.language, 'invalidEmbedObject')}\`\`\`${e.toString()}\`\`\``, message.channel);
  }
};

exports.config = {
  aliases: [],
  enabled: true
};

exports.help = {
  name: 'sendEmbed',
  description: 'Sends an embed message from the specified embed (JavaScript) object.',
  botPermission: '',
  userTextPermission: 'MANAGE_GUILD',
  userVoicePermission: '',
  usage: 'sendEmbed <embedObject>',
  example: [ 'sendEmbed {"title": "Hello", "description": "Isn\'t it cool?"}' ]
};
