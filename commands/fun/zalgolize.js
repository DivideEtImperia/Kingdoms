exports.exec = (Kingdoms, message, args) => {
  if (args.length < 1) {
    /**
     * The command was ran with invalid parameters.
     * @fires commandUsage
     */
    return Kingdoms.emit('commandUsage', message, this.help);
  }

  message.channel.send({
    embed: {
      color: Kingdoms.colors.BLUE,
      title: 'Zalgolized Text:',
      description: Kingdoms.methods.zalgolize(args.join(' '))
    }
  }).catch(e => {
    Kingdoms.log.error(e);
  });
};

exports.config = {
  aliases: [ 'zalgo' ],
  enabled: true
};

exports.help = {
  name: 'zalgolize',
  description: 'Sends the same message that you had sent, but zalgolized.',
  botPermission: '',
  userTextPermission: '',
  userVoicePermission: '',
  usage: 'zalgolize <text>',
  example: [ 'zalgolize It looks clumsy, but it\'s cool!' ]
};
