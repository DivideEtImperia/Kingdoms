exports.exec = async (Kingdoms, message, args) => {
  try {
    let user;
    if (message.mentions.users.size) {
      user = message.mentions.users.first();
    }
    else if (args.id) {
      user = await Kingdoms.fetchUser(args.id);
    }
    if (!user) {
      /**
      * The command was ran with invalid parameters.
      * @fires commandUsage
      */
      return Kingdoms.emit('commandUsage', message, this.help);
    }

    message.channel.send({
      embed: {
        color: Kingdoms.colors.RED,
        description: `**${message.author.tag}** has banned **${user.tag}** from this server.*`,
        footer: {
          text: '* Oh, just kidding! XD'
        }
      }
    }).catch(e => {
      Kingdoms.log.error(e);
    });
  }
  catch (e) {
    Kingdoms.log.error(e);
  }
};

exports.config = {
  aliases: [ 'fban' ],
  enabled: true,
  argsDefinitions: [
    { name: 'id', type: String, defaultOption: true }
  ]
};

exports.help = {
  name: 'fakeBan',
  description: 'Bans a user from the server*. Oh, not really though, just to mess with them.',
  botPermission: '',
  userTextPermission: '',
  userVoicePermission: '',
  usage: 'fakeBan [ @USER_MENTION | USER_ID ]',
  example: []
};
