exports.exec = (Kingdoms, message, args) => {
  message.channel.send({
    embed: {
      color: Kingdoms.colors.BLUE,
      title: `${args.length ? args.join(' ') : 'You'} is being removed.`,
      image: {
        url: 'https://resources.bastionbot.org/images/remove_button.gif'
      }
    }
  }).catch(e => {
    Kingdoms.log.error(e);
  });
};

exports.config = {
  aliases: [ 'delete' ],
  enabled: true
};

exports.help = {
  name: 'remove',
  description: 'Have fun removing stuff you don\'t like. Specify the item(s) you want to remove. Won\'t be real though!',
  botPermission: '',
  userTextPermission: '',
  userVoicePermission: '',
  usage: 'remove <text>',
  example: [ 'remove Humanity' ]
};
