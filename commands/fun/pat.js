const request = require('request-promise-native');

exports.exec = async (Kingdoms, message) => {
  try {
    let options = {
      url: 'http://api.giphy.com/v1/gifs/search',
      qs: {
        q: 'pat',
        api_key: 'dc6zaTOxFJmzC',
        limit: 10,
        offset: 0
      },
      json: true
    };

    let response = await request(options);

    if (response.data.length) {
      message.channel.send({
        embed: {
          color: Kingdoms.colors.BLUE,
          title: `A pat from ${message.author.tag}`,
          image: {
            url: response.data[Math.floor(Math.random() * response.data.length)].images.original.url
          },
          footer: {
            text: 'Powered by GIPHY'
          }
        }
      }).catch(e => {
        Kingdoms.log.error(e);
      });
    }
    else {
      return Kingdoms.emit('error', Kingdoms.strings.error(message.guild.language, 'notFound'), Kingdoms.strings.error(message.guild.language, 'notFound', true, 'image'), message.channel);
    }
  }
  catch (e) {
    if (e.response) {
      return Kingdoms.emit('error', e.response.statusCode, e.response.statusMessage, message.channel);
    }
    Kingdoms.log.error(e);
  }
};

exports.config = {
  aliases: [],
  enabled: true
};

exports.help = {
  name: 'pat',
  description: 'Pat someone!',
  botPermission: '',
  userTextPermission: '',
  userVoicePermission: '',
  usage: 'pat',
  example: [ 'pat' ]
};
