exports.exec = (Kingdoms, message) => {
  let users = message.mentions.users.map(u => u.username);
  if (users.length < 2) {
    /**
     * The command was ran with invalid parameters.
     * @fires commandUsage
     */
    return Kingdoms.emit('commandUsage', message, this.help);
  }

  let shippedName = '';
  for (let i = 0; i < users.length; i++) {
    shippedName += `${users[i].substring(0, users[i].length / 2)}`;
  }

  message.channel.send({
    embed: {
      color: Kingdoms.colors.BLUE,
      title: 'Shipped Users',
      description: `${users.join(' + ')} = **${shippedName}**`
    }
  }).catch(e => {
    Kingdoms.log.error(e);
  });
};

exports.config = {
  aliases: [],
  enabled: true
};

exports.help = {
  name: 'ship',
  description: 'Combines two or more mentioned user\'s names.',
  botPermission: '',
  userTextPermission: '',
  userVoicePermission: '',
  usage: 'ship <USER_MENTION> <USER_MENTION> [...USER_MENTION]',
  example: [ 'ship DivideEtImperia#0162 Ghiojo' ]
};
