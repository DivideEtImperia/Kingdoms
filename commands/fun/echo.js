exports.exec = (Kingdoms, message, args) => {
  if (args.length < 1) {
    /**
     * The command was ran with invalid parameters.
     * @fires commandUsage
     */
    return Kingdoms.emit('commandUsage', message, this.help);
  }

  message.channel.send({
    embed: {
      color: Kingdoms.colors.DEFAULT,
      description: args.join(' '),
      footer: {
        text: `${!Kingdoms.credentials.ownerId.includes(message.author.id) ? '' : Kingdoms.i18n.info(message.guild.language, 'endorsementMessage')}`
      }
    }
  }).catch(e => {
    Kingdoms.log.error(e);
  });
};

exports.config = {
  aliases: [ 'say' ],
  enabled: true
};

exports.help = {
  name: 'echo',
  description: 'Sends the same message that you had sent. Just like an echo!',
  botPermission: '',
  userTextPermission: 'MANAGE_GUILD',
  userVoicePermission: '',
  usage: 'echo <text>',
  example: [ 'echo Hello, world!' ]
};
