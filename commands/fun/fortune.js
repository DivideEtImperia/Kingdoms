const fortuneCookies = xrequire('./assets/fortuneCookies.json');

exports.exec = (Kingdoms, message) => {
  message.channel.send({
    embed: {
      color: Kingdoms.colors.BLUE,
      title: 'Fortune:',
      description: fortuneCookies[Math.floor(Math.random() * fortuneCookies.length)]
      // description: fortuneCookies.random()
    }
  }).catch(e => {
    Kingdoms.log.error(e);
  });
};

exports.config = {
  aliases: [ 'cookie' ],
  enabled: true
};

exports.help = {
  name: 'fortune',
  description: 'Shows you a fortune from a fortune cookie.',
  botPermission: '',
  userTextPermission: '',
  userVoicePermission: '',
  usage: 'fortune',
  example: []
};
