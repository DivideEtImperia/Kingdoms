const request = xrequire('request-promise-native');

exports.exec = async (Kingdoms, message, args) => {
  try {
    let string;
    if (args.length < 1) {
      string = message.author.tag;
    }
    else {
      string = args.join(' ');
    }

    let options = {
      url: `https://robohash.org/${encodeURIComponent(string)}?set=set0`,
      encoding: null
    };
    let response = await request(options);

    message.channel.send({
      files: [ { attachment: response } ]
    }).catch(e => {
      Kingdoms.log.error(e);
    });
  }
  catch (e) {
    if (e.response) {
      return Kingdoms.emit('error', e.response.statusCode, e.response.statusMessage, message.channel);
    }
    Kingdoms.log.error(e);
  }
};

exports.config = {
  aliases: [ 'botify' ],
  enabled: true
};

exports.help = {
  name: 'robotify',
  description: 'Generates a random robot image from the given string or your Discord tag if no string is specified.',
  botPermission: '',
  userTextPermission: '',
  userVoicePermission: '',
  usage: 'robotify [Random String]',
  example: [ 'robotify', 'robotify isotope cattle hazily muzzle' ]
};
