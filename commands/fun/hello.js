exports.exec = (Kingdoms, message) => {
  message.channel.send({
    embed: {
      color: Kingdoms.colors.BLUE,
      description: 'Hi! I\'m **Kingdoms**. \u{1F609}\n' +
                   'I\'m a BOT that is going to make your time in Kingdoms amazing!',
      footer: {
        text: `Type ${message.guild.prefix[0]}help to find out more about me.`
      }
    }
  }).catch(e => {
    Kingdoms.log.error(e);
  });
};

exports.config = {
  aliases: [ 'hi' ],
  enabled: true
};

exports.help = {
  name: 'hello',
  description: 'Get greetings from %Kingdoms%.',
  botPermission: '',
  userTextPermission: '',
  userVoicePermission: '',
  usage: 'hello',
  example: []
};
