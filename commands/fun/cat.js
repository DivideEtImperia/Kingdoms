const request = xrequire('request-promise-native');

exports.exec = async (Kingdoms, message) => {
  try {
    let options = {
      method: 'HEAD',
      url: 'https://thecatapi.com/api/images/get',
      followAllRedirects: true,
      resolveWithFullResponse: true
    };
    let response = await request(options);

    await message.channel.send({
      files: [ response.request.uri.href ]
    });
  }
  catch (e) {
    if (e.response) {
      return Kingdoms.emit('error', e.response.statusCode, e.response.statusMessage, message.channel);
    }
    Kingdoms.log.error(e);
  }
};

exports.config = {
  aliases: [],
  enabled: true
};

exports.help = {
  name: 'cat',
  description: 'Shows a random picture of a cat.',
  botPermission: '',
  userTextPermission: '',
  userVoicePermission: '',
  usage: 'cat',
  example: []
};
