const request = xrequire('request-promise-native');

exports.exec = async (Kingdoms, message) => {
  try {
    let baseURL = 'http://random.dog';

    let options = {
      url: `${baseURL}/woof`,
      json: true
    };
    let response = await request(options);

    await message.channel.send({
      files: [ `${baseURL}/${response}` ]
    });
  }
  catch (e) {
    if (e.response) {
      return Kingdoms.emit('error', e.response.statusCode, e.response.statusMessage, message.channel);
    }
    Kingdoms.log.error(e);
  }
};

exports.config = {
  aliases: [],
  enabled: true
};

exports.help = {
  name: 'dog',
  description: 'Shows a random picture of a dog.',
  botPermission: '',
  userTextPermission: '',
  userVoicePermission: '',
  usage: 'dog',
  example: []
};
