exports.exec = async (Kingdoms, message) => {
  try {
    let spinning = await message.channel.send({
      embed: {
        color: Kingdoms.colors.BLUE,
        description: `${message.author.tag} is spinning a fidget spinner...`,
        image: {
          url: 'https://i.imgur.com/KJJxVi4.gif'
        }
      }
    });

    let timeout = (Math.random() * (60 - 5 + 1)) + 5;
    setTimeout(() => {
      spinning.edit({
        embed: {
          color: Kingdoms.colors.BLUE,
          description: `${message.author.tag}, you spinned the fidget spinner for ${timeout.toFixed(2)} seconds.`
        }
      }).catch(e => {
        Kingdoms.log.error(e);
      });
    }, timeout * 1000);
  }
  catch (e) {
    Kingdoms.log.error(e);
  }
};

exports.config = {
  aliases: [ 'fidget' ],
  enabled: true
};

exports.help = {
  name: 'fidgetSpinner',
  description: 'Spins a fidget spinner for you and shows for how long it was spinning.',
  botPermission: '',
  userTextPermission: '',
  userVoicePermission: '',
  usage: 'fidgetSpinner',
  example: []
};
