const catFacts = xrequire('./assets/catFacts.json');

exports.exec = (Kingdoms, message) => {
  message.channel.send({
    embed: {
      color: Kingdoms.colors.BLUE,
      title: 'Cat Fact:',
      description: catFacts[Math.floor(Math.random() * catFacts.length)]
      // description: catFacts.random()
    }
  }).catch(e => {
    Kingdoms.log.error(e);
  });
};

exports.config = {
  aliases: [],
  enabled: true
};

exports.help = {
  name: 'catFact',
  description: 'Shows a random fact about cats.',
  botPermission: '',
  userTextPermission: '',
  userVoicePermission: '',
  usage: 'catfact',
  example: []
};
