exports.exec = async (Kingdoms, message) => {
  try {
    if (message.guild.voiceConnection) {
      if (!message.guild.voiceConnection.channel.permissionsFor(message.member).has(this.help.userTextPermission)) {
        /**
        * User has missing permissions.
        * @fires userMissingPermissions
        */
        return Kingdoms.emit('userMissingPermissions', this.help.userTextPermission);
      }

      if (message.guild.voiceConnection.speaking) {
        /**
        * Error condition is encountered.
        * @fires error
        */
        return Kingdoms.emit('error', '', Kingdoms.i18n.error(message.guild.language, 'isSpeaking'), message.channel);
      }

      if (!message.guild.voiceConnection.channel.speakable) {
        /**
        * Kingdoms has missing permissions.
        * @fires kingdomsMissingPermissions
        */
        return Kingdoms.emit('kingdomsMissingPermissions', 'SPEAK', message);
      }

      message.guild.voiceConnection.playFile('./assets/airhorn.wav', { passes: (Kingdoms.configurations.music && Kingdoms.configurations.music.passes) || 1, bitrate: 'auto' });
    }
    else if (message.member.voiceChannel) {
      if (!message.member.voiceChannel.permissionsFor(message.member).has(this.help.userTextPermission)) {
        /**
        * User has missing permissions.
        * @fires userMissingPermissions
        */
        return Kingdoms.emit('userMissingPermissions', this.help.userTextPermission);
      }

      if (!message.member.voiceChannel.joinable) {
        /**
        * Bastion has missing permissions.
        * @fires kingdomsMissingPermissions
        */
        return Kingdoms.emit('kingdomsMissingPermissions', 'CONNECT', message);
      }

      if (!message.member.voiceChannel.speakable) {
        /**
        * Bastion has missing permissions.
        * @fires bastionMissingPermissions
        */
        return Kingdoms.emit('kingdomsMissingPermissions', 'SPEAK', message);
      }

      let connection = await message.member.voiceChannel.join();

      connection.on('error', Kingdoms.log.error);
      connection.on('failed', Kingdoms.log.error);

      const dispatcher = connection.playFile('./assets/airhorn.wav', { passes: (Kingdoms.configurations.music && Kingdoms.configurations.music.passes) || 1, bitrate: 'auto' });

      dispatcher.on('error', error => {
        Kingdoms.log.error(error);
      });

      dispatcher.on('end', () => {
        connection.channel.leave();
      });
    }
    else {
      /**
      * Error condition is encountered.
      * @fires error
      */
      return Kingdoms.emit('error', '', Kingdoms.i18n.error(message.guild.language, 'eitherOneInVC'), message.channel);
    }
  }
  catch (e) {
    Kingdoms.log.error(e);
  }
};

exports.config = {
  aliases: [ 'horn' ],
  enabled: true
};

exports.help = {
  name: 'airhorn',
  description: 'Plays an airhorn in a voice channel.',
  botPermission: '',
  userTextPermission: 'MUTE_MEMBERS',
  userVoicePermission: '',
  usage: 'airhorn',
  example: []
};
