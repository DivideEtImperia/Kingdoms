exports.exec = (Kingdoms, message, args) => {
  if (args.length < 1) {
    /**
     * The command was ran with invalid parameters.
     * @fires commandUsage
     */
    return Kingdoms.emit('commandUsage', message, this.help);
  }

  message.channel.send({
    embed: {
      color: Kingdoms.colors.BLUE,
      title: 'Reversed Text:',
      description: args.join(' ').split('').reverse().join('')
    }
  }).catch(e => {
    Kingdoms.log.error(e);
  });
};

exports.config = {
  aliases: [ 'rev' ],
  enabled: true
};

exports.help = {
  name: 'reverse',
  description: 'Sends the same message that you had sent but reversed.',
  botPermission: '',
  userTextPermission: '',
  userVoicePermission: '',
  usage: 'reverse <text>',
  example: [ 'reverse !looc si sihT' ]
};
