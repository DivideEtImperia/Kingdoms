const request = xrequire('request-promise-native');

exports.exec = async (Kingdoms, message, args) => {
  try {
    if (!args.player) {
      /**
       * The command was ran with invalid parameters.
       * @fires commandUsage
       */
      return Kingdoms.emit('commandUsage', message, this.help);
    }

    let options = {
      uri: 'https://api.roblox.com/users/get-by-username',
      headers: {
        'User-Agent': `Kingdoms/${Kingdoms.package.version} (${Kingdoms.user.tag}; ${Kingdoms.user.id}) http://home.kingdoms.network`
      },
      qs: {
        username: encodeURIComponent(args.player)
      },
      json: true
    };

    let response = await request(options);

    if (response.errorMessage) {
      return Kingdoms.emit('error', 'Roblox', response.errorMessage, message.channel);
    }

    message.channel.send({
      embed: {
        color: Kingdoms.colors.BLUE,
        title: 'Roblox Player',
        description: response.IsOnline ? 'Online' : 'Offline',
        fields: [
          {
            name: 'Player Username',
            value: response.Username,
            inline: true
          },
          {
            name: 'Player ID',
            value: response.Id,
            inline: true
          }
        ],
        footer: {
          text: 'Powered by Roblox'
        }
      }
    }).catch(e => {
      Kingdoms.log.error(e);
    });
  }
  catch (e) {
    Kingdoms.log.error(e);
  }
};

exports.config = {
  aliases: [],
  enabled: true,
  argsDefinitions: [
    { name: 'player', type: String, multiple: true, defaultOption: true }
  ]
};

exports.help = {
  name: 'roblox',
  description: 'Get info of any Roblox player.',
  botPermission: '',
  userTextPermission: '',
  userVoicePermission: '',
  usage: 'roblox <ROBLOX_USERNAME>',
  example: [ 'roblox Acramus' ]
};
