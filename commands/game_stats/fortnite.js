const request = xrequire('request-promise-native');

exports.exec = async (Kingdoms, message, args) => {
  try {
    if (!args.player) {
      /**
      * The command was ran with invalid parameters.
      * @fires commandUsage
      */
      return Kingdoms.emit('commandUsage', message, this.help);
    }

    // If user doesn't provide the platform, default to PC
    if (!args.platform) {
      args.platform = 'pc';
    }
    else {
      let platforms = [ 'pc', 'xbl', 'psn' ]; // Available platforms for the game
      // If the platform is not valid, return the available platforms
      if (!platforms.includes(args.platform = args.platform.toLowerCase())) {
        return Kingdoms.emit('error', '', Kingdoms.i18n.error(message.guild.language, 'invalidPlatform', `${platforms.join(', ').toUpperCase()}`), message.channel);
      }
    }

    let options = {
      uri: `https://api.fortnitetracker.com/v1/profile/${args.platform}/${encodeURIComponent(args.player.join(' '))}`,
      headers: {
        'TRN-Api-Key': Kingdoms.credentials.fortniteAPIKey,
        'User-Agent': `Kingdoms/${Kingdoms.package.version} (${Kingdoms.user.tag}; ${Kingdoms.user.id}) http://home.kingdoms.network`
      },
      json: true
    };

    let player = await request(options);
    if (player.error) {
      return Kingdoms.emit('error', 'Error', player.error, message.channel);
    }

    let stats = player.lifeTimeStats.map(stat => {
      return {
        name: stat.key,
        value: stat.value,
        inline: true
      };
    });

    message.channel.send({
      embed: {
        color: Kingdoms.colors.BLUE,
        author: {
          name: player.epicUserHandle
        },
        title: `Fortnite Stats - ${player.platformNameLong}`,
        fields: stats,
        thumbnail: {
          url: 'https://i.imgur.com/dfgwClZ.jpg'
        },
        footer: {
          text: 'Powered by Tracker Network'
        }
      }
    }).catch(e => {
      Kingdoms.log.error(e);
    });
  }
  catch (e) {
    if (e.name === 'StatusCodeError') {
      return Kingdoms.emit('error', e.statusCode, e.error.message, message.channel);
    }
    Kingdoms.log.error(e);
  }
};

exports.config = {
  aliases: [],
  enabled: true,
  argsDefinitions: [
    { name: 'player', type: String, multiple: true, defaultOption: true },
    { name: 'platform', type: String, alias: 'p', defaultValue: 'PC' }
  ]
};

exports.help = {
  name: 'fortnite',
  description: 'Get stats of any Fortnite player.',
  botPermission: '',
  userTextPermission: '',
  userVoicePermission: '',
  usage: 'fortnite <EPIC_NICKNAME> [ -p <PLATFORM> ]',
  example: [ 'fortnite DivideEtImperia -p PC' ]
};
