const question = xrequire('./assets/thisOrThat.json');

exports.exec = (Kingdoms, message) => {
  message.channel.send({
    embed: {
      color: Kingdoms.colors.BLUE,
      description: question[Math.floor(Math.random() * question.length)]
    }
  }).catch(e => {
    Kingdoms.log.error(e);
  });
};

exports.config = {
  aliases: [ 'thisthat' ],
  enabled: true
};

exports.help = {
  name: 'thisOrThat',
  description: 'Asks a this or that question. See how you and your friends choose!',
  botPermission: '',
  userTextPermission: '',
  userVoicePermission: '',
  usage: 'thisOrThat',
  example: []
};
