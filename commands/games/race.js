const ProgressBar = xrequire('./utils/progress');

exports.exec = async (Kingdoms, message) => {
  try {
    let racers = [ [], [] ];
    const STEPS = 20;
    for (let i = 0; i < racers.length; i++) {
      racers[i].length = STEPS;
      for (let j = 0; j < STEPS; j++) {
        racers[i][j] = '-\u2003';
      }
    }

    const kingdom = new ProgressBar(':bar', {
      incomplete: '-\u2003',
      complete: '-\u2003',
      head: '\u2003‣\u2003',
      total: 20
    });
    const racer = new ProgressBar(':bar', {
      incomplete: '-\u2003',
      complete: '-\u2003',
      head: '\u2003‣\u2003',
      total: 20
    });

    let raceStatusMessage = await message.channel.send({
      embed: {
        color: Kingdoms.colors.BLUE,
        title: 'Race',
        fields: [
          {
            name: Kingdoms.user.tag,
            value: `:vertical_traffic_light: ${racers[0].join('')}:checkered_flag:`
          },
          {
            name: message.author.tag,
            value: `:vertical_traffic_light: ${racers[1].join('')}:checkered_flag:`
          }
        ]
      }
    });

    let timer = setInterval(() => {
      for (let i = 0; i < Math.floor(Math.random() * (5 - 1 + 1) + 1); i++) {
        racer.tick();
      }
      for (let i = 0; i < Math.floor(Math.random() * (5 - 1 + 1) + 1); i++) {
        kingdom.tick();
      }

      if (kingdom.lastDraw) {
        let result = 'Race ',
          progressKingdom = `:vertical_traffic_light: ${kingdom.lastDraw}:checkered_flag:`,
          progressRacer = `:vertical_traffic_light: ${racer.lastDraw}:checkered_flag:`;
        if (kingdom.complete && !racer.complete) {
          result += 'Ended';
          progressKingdom = `:vertical_traffic_light: ${kingdom.lastDraw}:checkered_flag: :trophy:`;
        }
        else if (!kingdom.complete && racer.complete) {
          result += 'Ended';
          progressRacer = `:vertical_traffic_light: ${racer.lastDraw}:checkered_flag: :trophy:`;
        }
        else if (kingdom.complete && racer.complete) {
          result += 'Ended - Draw';
        }
        raceStatusMessage.edit({
          embed: {
            color: Kingdoms.colors.BLUE,
            title: result,
            fields: [
              {
                name: Kingdoms.user.tag,
                value: progressKingdom
              },
              {
                name: message.author.tag,
                value: progressRacer
              }
            ]
          }
        }).catch(e => {
          Kingdoms.log.error(e);
        });
      }
      if (kingdom.complete || racer.complete) {
        clearInterval(timer);
      }
    }, 1000);
  }
  catch (e) {
    Kingdoms.log.error(e);
  }
};

exports.config = {
  aliases: [],
  enabled: true
};

exports.help = {
  name: 'race',
  description: 'Start a race against %Kingdoms%.',
  botPermission: '',
  userTextPermission: '',
  userVoicePermission: '',
  usage: 'race',
  example: [ 'race' ]
};
