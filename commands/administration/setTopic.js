exports.exec = async (Kingdoms, message, args) => {
  try {
    let channel = message.mentions.channels.first();
    let topic;
    if (!channel) {
      channel = message.channel;
      topic = args.join(' ');
    }
    else {
      topic = args.slice(1).join(' ').trim();
    }

    if (!channel.permissionsFor(message.member).has(this.help.userTextPermission)) {
      /**
      * User has missing permissions.
      * @fires userMissingPermissions
      */
      return Kingdoms.emit('userMissingPermissions', this.help.userTextPermission);
    }
    if (!channel.permissionsFor(message.guild.me).has(this.help.botPermission)) {
      /**
      * Kingdoms has missing permissions.
      * @fires kingdomMissingPermissions
      */
      return Kingdoms.emit('kingdomsMissingPermissions', this.help.botPermission, message);
    }

    await channel.setTopic(topic);

    await message.channel.send({
      embed: {
        color: Kingdoms.colors.ORANGE,
        description: Kingdoms.i18n.info(message.guild.language, 'updateChannelTopic', message.author.tag, channel.name, channel.topic)
      }
    });
  }
  catch (e) {
    Kingdoms.log.error(e);
  }
};

exports.config = {
  aliases: [ 'sett' ],
  enabled: true
};

exports.help = {
  name: 'setTopic',
  description: 'Sets channel topic of the specified text channel.',
  botPermission: 'MANAGE_CHANNELS',
  userTextPermission: 'MANAGE_CHANNELS',
  userVoicePermission: '',
  usage: 'setTopic [#channel-mention] [Channel Topic]',
  example: [ 'setTopic #channel-name New Topic', 'setTopic Server Stuff', 'setTopic' ]
};
