exports.exec = async (Kingdoms, message, args) => {
  try {
    if (!args.description) {
      /**
       * The command was ran with invalid parameters.
       * @fires commandUsage
       */
      return Kingdoms.emit('commandUsage', message, this.help);
    }

    let guildModel = await Kingdoms.database.models.guild.findOne({
      attributes: [ 'suggestionChannel' ],
      where: {
        guildID: message.guild.id
      }
    });

    let suggestionChannel;
    if (guildModel && guildModel.dataValues.suggestionChannel) {
      suggestionChannel = message.guild.channels.filter(channel => channel.type === 'text').get(guildModel.dataValues.suggestionChannel);
    }

    if (!suggestionChannel) {
      suggestionChannel = message.channel;
    }

    let suggestion = await suggestionChannel.send({
      embed: {
        title: 'Suggestion',
        description: args.description.join(' '),
        image: {
          url: (message.attachments.size && message.attachments.first().height && message.attachments.first().url) || null
        },
        footer: {
          text: `Suggested by ${message.author.tag}`
        }
      }
    });

    // Delete user's message
    if (message.deletable) {
      message.delete().catch(() => {});
    }

    // Add reactions for voting
    await suggestion.react('👍');
    await suggestion.react('👎');
  }
  catch (e) {
    Kingdoms.log.error(e);
  }
};

exports.config = {
  aliases: [],
  enabled: true,
  argsDefinitions: [
    { name: 'description', type: String, multiple: true, defaultOption: true }
  ]
};

exports.help = {
  name: 'suggest',
  description: 'Submit a suggestion',
  botPermission: '',
  userTextPermission: '',
  userVoicePermission: '',
  usage: 'suggest <SUGGESTION>',
  example: [ 'suggest Add a feedback page to the website.' ]
};
