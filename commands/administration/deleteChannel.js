exports.exec = async (Kingdoms, message, args) => {
  try {
    let channel = message.mentions.channels.first();
    if (!channel) {
      channel = message.channel;
      if (args.id) {
        channel = message.guild.channels.get(args.id);
      }
      else if (args.name) {
        channel = message.guild.channels.find('name', args.name.join(' '));
      }
      if (!channel) {
        /**
        * Error condition is encountered.
        * @fires error
        */
        return Kingdoms.emit('error', '', Kingdoms.i18n.error(message.guild.language, 'channelNotFound'), message.channel);
      }
    }

    if (!channel.permissionsFor(message.member).has(this.help.userTextPermission)) {
      /**
      * User has missing permissions.
      * @fires userMissingPermissions
      */
      return Kingdoms.emit('userMissingPermissions', this.help.userTextPermission);
    }
    if (!channel.permissionsFor(message.guild.me).has(this.help.botPermission)) {
      /**
      * Kingdoms has missing permissions.
      * @fires kingdomsMissingPermissions
      */
      return Kingdoms.emit('kingdomsMissingPermissions', this.help.botPermission, message);
    }

    await channel.delete();

    if (channel.id === message.channel.id) return;

    await message.channel.send({
      embed: {
        color: Kingdoms.colors.RED,
        description: Kingdoms.i18n.info(message.guild.language, 'deleteChannel', message.author.tag, channel.type, channel.name)
      }
    });
  }
  catch (e) {
    Kingdoms.log.error(e);
  }
};

exports.config = {
  aliases: [ 'deletec' ],
  enabled: true,
  argsDefinitions: [
    { name: 'mention', type: String, alias: 'm', defaultOption: true },
    { name: 'id', type: String, alias: 'i' },
    { name: 'name', type: String, alias: 'n', multiple: true }
  ]
};

exports.help = {
  name: 'deleteChannel',
  description: 'Deletes the specified text or voice channel.',
  botPermission: 'MANAGE_CHANNELS',
  userTextPermission: 'MANAGE_CHANNELS',
  userVoicePermission: '',
  usage: 'deleteChannel [ [-m] #channel-mention | -i CHANNEL_ID | -n Channel Name ]',
  example: [ 'deleteChannel -m #channel-name', 'deleteChannel -i 384593740808847361', 'deleteChannel -n lounge' ]
};
