exports.exec = async (Kingdoms, message, args) => {
  try {
    if (!args.name) {
      /**
      * The command was ran with invalid parameters.
      * @fires commandUsage
      */
      return Kingdoms.emit('commandUsage', message, this.help);
    }

    let minLength = 2, maxLength = 100;
    args.name = args.name.join(' ');
    if (args.name.length < minLength || args.name.length > maxLength) {
      /**
      * Error condition is encountered.
      * @fires error
      */
      return Kingdoms.emit('error', '', Kingdoms.i18n.error(message.guild.language, 'channelNameLength', minLength, maxLength), message.channel);
    }

    let channelType;
    if (!args.text && args.voice) {
      channelType = 'voice';
    }
    else {
      channelType = 'text';
      args.name = args.name.replace(' ', '-');
    }

    let channel = await message.guild.createChannel(args.name, channelType);

    await message.channel.send({
      embed: {
        color: Kingdoms.colors.GREEN,
        description: Kingdoms.i18n.info(message.guild.language, 'createChannel', message.author.tag, channelType, channel.name),
        footer: {
          text: `ID: ${channel.id}`
        }
      }
    });
  }
  catch (e) {
    Kingdoms.log.error(e);
  }
};

exports.config = {
  aliases: [ 'createc' ],
  enabled: true,
  argsDefinitions: [
    { name: 'name', type: String, alias: 'n', multiple: true, defaultOption: true },
    { name: 'text', type: Boolean, alias: 't' },
    { name: 'voice', type: Boolean, alias: 'v' }
  ]
};

exports.help = {
  name: 'createChannel',
  description: 'Creates a new text or voice channel.',
  botPermission: 'MANAGE_CHANNELS',
  userTextPermission: 'MANAGE_CHANNELS',
  userVoicePermission: 'MANAGE_CHANNELS',
  usage: 'createChannel [-t | -v] <Channel Name>',
  example: [ 'createChannel -t Text Channel Name', 'createChannel -v Voice Channel Name', 'createChannel Text Channel Name' ]
};
