Write-Host "[Kingdoms]: Checking KingdomsBot system..."
If (Test-Path ".\index.js") {
  Write-Host "[Kingdoms]: System check successful."
  Write-Host

  Write-Host "[Kingdoms]: Booting up..."
  Write-Host

  node -r ./utils/globals.js .
}
Else {
  Write-Host "[Kingdoms]: System check failed."
  Write-Host

  Write-Host "[Kingdoms]: Check if you have installed KingdomsBot correctly."
  Write-Host "[Kingdoms]: Ask DivideEtImperia or check the wiki http://home.kingdoms.network/info/"
}
Write-Host
