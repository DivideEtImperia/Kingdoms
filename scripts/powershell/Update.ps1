Function Pass-Step {
  Write-Host
  Write-Host "[Kingdoms]: Done."
  Write-Host
}

Function Exit-Kingdoms-Updater {
  Write-Host
  Write-Host "[Kingdoms]: Get Help/Support in Kingdoms: http://discord.kingdoms.network/"
  Write-Host

  Exit-PSSession
}


# Check if Kingdoms is installed correctly
Write-Host "[Kingdoms]: Checking Kingdoms system..."
If (-Not (Test-Path ".git")) {
  Write-Host "[Kingdoms]: System check failed."
  Write-Host

  Write-Host "[Kingdoms]: Check if you have installed Kingdoms correctly."
  Write-Host "[Kingdoms]: Ask DivideEtImperia or check the wiki http://home.kingdoms.network/info/"

  Exit-Kingdoms-Updater
}
Write-Host "[Kingdoms]: System check successful."
Write-Host


# Pull new changes to Kingdoms from GitLab
Write-Host "[Kingdoms]: Updating Kingdoms..."
Write-Host

git pull
If (-Not ($?)) {
  Write-Host "[Kingdoms]: Unable to update Kingdoms, error while updating Kingdoms."
  Write-Host "[Kingdoms]: Contact Kingdoms Support for help."
  Write-Host

  Exit-Bastion-Updater
}

Pass-Step


# Update global FFmpeg binaries
Write-Host "[Kingdoms]: Updating FFmpeg..."
Write-Host

choco upgrade ffmpeg -y
If (-Not ($?)) {
  Write-Host "[Kingdoms]: Unable to update Kingdoms, error while updating FFmpeg."
  Write-Host "[Kingdoms]: Try updating it manually: choco upgrade ffmpeg -y"
  Write-Host "[Kingdoms]: Contact Kingdoms Support for further help."

  Exit-Kingdoms-Updater
}

Pass-Step


# Update Kingdoms dependencies
Write-Host "[Kingdoms]: Updating dependencies..."
Write-Host

Remove-Item -Path ".\node_modules", ".\package-lock.json" -Force -Recurse -ErrorAction SilentlyContinue
yarn install --production --no-lockfile
If (-Not ($?)) {
  Write-Host "[Kingdoms]: Unable to update Kingdoms, error while updating dependencies."
  Write-Host "[Kingdoms]: Try updating it manually: yarn install --production --no-lockfile"
  Write-Host "[Kingdoms]: Contact Kingdoms Support for further help."

  Exit-Kingdoms-Updater
}

Pass-Step


# Update was successful
Write-Host "[Kingdoms]: Kingdoms was successfully updated."
Write-Host

Write-Host "[Kingdoms]: Ready to boot up and start running!"

Exit-Kingdoms-Updater
