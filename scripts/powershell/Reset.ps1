$caption = "[Kingdoms]: Are you sure you want to reset Kingdoms?"
$description = "[Kingdoms]: Resetting Kingdoms will remove ALL the saved data.
 "

$choices = New-Object Collections.ObjectModel.Collection[Management.Automation.Host.ChoiceDescription]
$choices.Add((
  New-Object Management.Automation.Host.ChoiceDescription `
    -ArgumentList `
      "&Yes",
      "Yes, I'm sure I want to reset all the saved data of Kingdoms."
))
$choices.Add((
  New-Object Management.Automation.Host.ChoiceDescription `
    -ArgumentList `
      "&No",
      "No, I don't want to delete the saved data.
      "
))

$decision = $Host.UI.PromptForChoice($caption, $description, $choices, 1)
Write-Host

If ($decision -eq 0) {
  Write-Host "[Kingdoms]: Resetting Kingdoms..."
  Remove-Item -Path ".\data\kingdoms.db" -Force -Recurse -ErrorAction SilentlyContinue
  Write-Host "[Kingdoms]: Done."
  Write-Host

  Write-Host "[Kingdoms]: All the saved data was removed from Kingdoms."
}
Else {
  Write-Host "[Kingdoms]: The operation was cancelled."
  Write-Host "[Kingdoms]: None of your data was removed."
}


Write-Host
Write-Host "[Kingdoms]: Get Help/Support in Kingdoms: http://discord.kingdoms.network/"
Write-Host
