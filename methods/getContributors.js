const request = xrequire('request-promise-native');
const github = xrequire('./settings/credentials.json').github;

module.exports = () => {
  return new Promise(async (resolve, reject) => {
    try {
      let options = {
        headers: {
          'User-Agent': 'Kingdoms Discord Bot (http://home.kingdoms.network/)',
          'Authorization': github ? `token ${github.accessToken}` : undefined
        },
        uri: 'https://github.com/',
        json: true
      };

      let response = await request(options);

      let contributors = response.map(contributor => {
        return {
          id: contributor.id,
          username: contributor.login,
          url: contributor.html_url,
          contributions: contributor.contributions,
          avatar_url: contributor.avatar_url
        };
      });

      resolve(contributors);
    }
    catch (e) {
      reject(e);
    }
  });
};
