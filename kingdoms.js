const Tesseract = xrequire('tesseract');
const KINGDOMS = new Tesseract.Client({
  configurations: xrequire('./settings/config.json'),
  credentials: xrequire('./settings/credentials.json'),
  disabledEvents: [
    'USER_NOTE_UPDATE',
    'TYPING_START',
    'RELATIONSHIP_ADD',
    'RELATIONSHIP_REMOVE'
  ]
});

if (KINGDOMS.shard) process.title = `Kingdoms-Shard-${KINGDOMS.shard.id}`;
else process.title = 'KingdomsBot';

KINGDOMS.package = xrequire('./package.json');
KINGDOMS.Constants = Tesseract.Constants;
KINGDOMS.colors = Tesseract.Constants.Colors;
KINGDOMS.permissions = Tesseract.Permissions.FLAGS;

// xrequire('./prototypes/Array.prototype');
xrequire('./prototypes/String.prototype');
xrequire('./prototypes/Number.prototype');

const WebhookHandler = xrequire('./handlers/webhookHandler.js');
KINGDOMS.webhook = new WebhookHandler(KINGDOMS.credentials.webhooks);
KINGDOMS.log = xrequire('./handlers/logHandler');
KINGDOMS.methods = xrequire('./handlers/methodHandler');

const StringHandler = xrequire('./handlers/stringHandler');
KINGDOMS.i18n = new StringHandler();

const Sequelize = xrequire('sequelize');
KINGDOMS.database = new Sequelize(KINGDOMS.credentials.database.URI, {
  operatorsAliases: false,
  logging: false
});
KINGDOMS.database.authenticate().then(() => {
  // Populate Database/Implement model definitions
  xrequire('./utils/models')(Sequelize, KINGDOMS.database);

  // Load Kingdoms Events
  xrequire('./handlers/eventHandler')(KINGDOMS);

  // Load Kingdoms Modules
  const Modules = xrequire('./handlers/moduleHandler');
  KINGDOMS.commands = Modules.commands;
  KINGDOMS.aliases = Modules.aliases;

  // Start Bastion
  KINGDOMS.login(KINGDOMS.credentials.token).then(() => {
    /**
     * Using <Model>.findOrCreate() won't require the use of
     * <ModelInstance>.save() but <Model>.findOrBuild() is used instead because
     * <Model>.findOrCreate() creates a race condition where a matching row is
     * created by another connection after the `find` but before the `insert`
     * call. However, it is not always possible to handle this case in SQLite,
     * specifically if one transaction inserts and another tries to select
     * before the first one has committed. TimeoutError is thrown instead.
     */
    KINGDOMS.database.models.settings.findOrBuild({
      where: {
        botID: KINGDOMS.user.id
      }
    }).spread((settingsModel, initialized) => {
      if (initialized) {
        return settingsModel.save();
      }
    }).catch(KINGDOMS.log.error);
  }).catch(e => {
    KINGDOMS.log.error(e.toString());
    process.exit(1);
  });
}).catch(err => {
  KINGDOMS.log.error(err);
});

process.on('unhandledRejection', rejection => {
  /* eslint-disable no-console */
  console.warn('\n[unhandledRejection]');
  console.warn(rejection);
  console.warn('[/unhandledRejection]\n');
  /* eslint-enable no-console */
});
